# Yl954 Mini Project 7
This project ingests data into Vector database (Qdrant), performs queries and aggregations, and visualizea output.
https://qdrant.tech/documentation/quick-start/

## Author
Yuanzhi Lou

## Download Qdrant Docker Image
1. ```docker pull qdrant/qdrant```

## Create Rust Application
1. ```cargo init```
2. Add qdrant-client to ```Cargo.toml```
```
[dependencies]
qdrant-client = "1.8.0"
```
3. Replace the content of ```src/main.rs``` with vector database operation code

## Ingest Data
![](ingest.png)

## Perform Queries
![](query.png)

## Visualize Output
![](result.png)

## Delete Docker Image
1. Get image ID by ```sudo docker images```
2. Delete image by ```sudo docker rmi <image_ID>```

## Create and run Container Locally
```sudo docker run -p 6333:6333 -p 6334:6334 qdrant/qdrant```\
Note: ```-d: run in the background```
![](server.png)

## Start or Stop or Delete Container
1. View all containers: ```sudo docker ps -a```
2. Start: ```sudo docker start container_id```
3. Stop: ```sudo docker stop container_id```
4. Delete: ```sudo docker rm container_id```
