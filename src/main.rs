use qdrant_client::qdrant::{vectors_config::Config, VectorParams, VectorsConfig};
use qdrant_client::qdrant::PointStruct;
use qdrant_client::qdrant::SearchPoints;
use qdrant_client::qdrant::Distance;
use qdrant_client::qdrant::CreateCollection;
use qdrant_client::client::QdrantClient;
use anyhow::Result;
use serde_json::json;

#[tokio::main]
async fn main() -> Result<()> {
    let client = QdrantClient::from_url("http://localhost:6334").build()?;
    let collection = "my_collection";

    let _ = client.delete_collection(collection).await;
    client
    .create_collection(&CreateCollection {
        collection_name: collection.to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Dot.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    })
    .await?;

    // Ingest data into Vector database
    let points = vec![
        PointStruct::new(
            1,
            vec![0.05, 0.61, 0.76, 0.74],
            json!(
                {"city": "Berlin"}
            )
            .try_into()
            .unwrap(),
        ),
        PointStruct::new(
            2,
            vec![0.19, 0.81, 0.75, 0.11],
            json!(
                {"city": "London"}
            )
            .try_into()
            .unwrap(),
        ),
    ];
    client
        .upsert_points_blocking(collection.to_string(), None, points, None)
        .await?;

    // Perform queries and aggregations
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection.to_string(),
            vector: vec![0.2, 0.1, 0.9, 0.7],
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;

    dbg!(search_result);

    Ok(())
}
